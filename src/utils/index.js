/**
 * 清除登录信息
 */
export function clearLoginInfo () {
    window.sessionStorage.clear(),
    this.$router.push('/login')
  }
  