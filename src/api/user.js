import request from '@/utils/request.js'
import qs from 'qs'

// 获取手记导航数据
export function authtication(loginForm) {
    return request({
        url: '/login',
        method: 'post',
        data: loginForm
      })
}

export function listUser(queryInfo) {
  return request({
      url: '/users',
      method: 'get',
      params: queryInfo
    })
}

export function deleteUser(id) {
  return request({
      url: `/users/` + id,
      method: 'delete'
    })
}

export function saveUser(userForm) {
  console.log(userForm)
  return request({
      url: '/users',
      method: 'post',
      data: userForm
    })
}

export function updateUser(userForm) {
  return request({
      url: `/users/` + userForm.id,
      method: 'put',
      data: userForm
    })
}

export function updateUserState(id, type) {
  return request({
      url: `users/` + id + `/state/` + type,
      method: 'put'
    })
}
