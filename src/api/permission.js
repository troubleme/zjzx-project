import request from '@/utils/request.js'

// 获取手记导航数据
export function getMenus() {
    return request({
        url: '/menus',
        method: 'get'
      })
}